import { useEffect, useState } from "react";
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardMedia from "@mui/material/CardMedia";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import { Box } from "@mui/material";
import { AiFillStar } from "react-icons/ai";
import Navbar from "@/components/navbar";

export default function Electronics() {
  const [jeweLerydata, setJewelerydata] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [cart, setCart] = useState([]);

  const datajewelery = () => {
    fetch("https://fakestoreapi.com/products/category/electronics")
      .then((res) => res.json())
      .then((data) => {
        setJewelerydata(data);
        setIsLoading(false);
        console.log(data, "jewelery");
      })
      .catch((error) => {
        setIsLoading(false);
        console.error("Error fetching data:", error);
      });
  };

  const addToCart = (item) => {
    setCart((prevCart) => [...prevCart, item]);
    console.log(item);
  };

  const cartItemCount = cart.length;
  console.log(cartItemCount);

  useEffect(() => {
    datajewelery();
  }, []);

  
  return (
    <Box>
      <Navbar itemCount={cartItemCount} cart={cart} setCart={setCart} />
      <Box className="grid lg:grid-cols-4 md:grid-cols-2 sm:grid-cols-2 gap-5 lg:px-[8rem] px-0 mt-7">
        {isLoading ? (
          <>
            {[...Array(8)].map((_, index) => (
              <Card
                key={index}
                style={{ maxWidth: "100%", height: " 60vh" }}
                className="relative"
              >
                <CardContent>
                  <div style={{ height: "300px", background: "#ddd" }} />
                  <Typography
                    sx={{ margin: "0.75rem 0.5rem 0.5rem" }}
                    variant="h6"
                    color={"#FF9933"}
                  >
                    &nbsp;
                  </Typography>
                  <Box
                    sx={{
                      display: "flex",
                      gap: "10px",
                      margin: "0.75rem 0.5rem 0.5rem",
                    }}
                  >
                    <Box>
                      <AiFillStar
                        Size={20}
                        color="#FFFF00"
                        style={{ marginTop: "4px" }}
                      />
                    </Box>
                    <Box>
                      <Typography>&nbsp;</Typography>
                    </Box>
                    <Box>
                      <Typography>&nbsp;</Typography>
                    </Box>
                  </Box>
                  <div style={{ height: "40px", background: "#ddd" }} />
                </CardContent>
              </Card>
            ))}
          </>
        ) : (
          jeweLerydata?.map((item, index) => (
            <Card
              key={index}
              style={{ maxWidth: "100%", height: " 60vh" }}
              className="relative"
            >
              <CardHeader title={item?.category} className=" text-base" />
              <Box className="px-10">
                <CardMedia
                  component="img"
                  image={item?.image}
                  alt="Paella dish"
                  style={{
                    height: "250px",
                    width: "100%",
                    objectFit: "contain",
                  }}
                />
              </Box>

              <Typography
                sx={{ margin: "0.75rem 0.5rem 0.5rem" }}
                variant="h6"
                color={"#FF9933"}
              >
                {item?.title}
              </Typography>

              <Box
                sx={{
                  display: "flex",
                  gap: "5px",
                  margin: "0.75rem 0.5rem 0.5rem",
                }}
              >
                <Box>
                  <AiFillStar
                    Size={20}
                    color="#FFFF00"
                    style={{ marginTop: "4px" }}
                  />
                </Box>
                <Box>
                  <Typography>{item?.rating?.rate}</Typography>
                </Box>
                <Box>
                  <Typography>{item?.rating?.count}&nbsp;reviews</Typography>
                </Box>
              </Box>

              <div className="absolute bottom-4  px-4  w-full">
                <div className="flex justify-between items-center w-full">
                  <Typography>${item.price}</Typography>
                  <button
                    className="bg-blue-400 text-white rounded-md shadow-md p-2 hover:bg-white hover:border-blue-600 hover:text-blue-500 border"
                    onClick={() => addToCart(item)}
                  >
                    Add to card
                  </button>
                </div>
              </div>
            </Card>
          ))
        )}
      </Box>
    </Box>
  );
}
