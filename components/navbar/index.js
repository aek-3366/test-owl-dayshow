import React, { useState } from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Badge from "@mui/material/Badge";
import MenuItem from "@mui/material/MenuItem";
import Menu from "@mui/material/Menu";
import AccountCircle from "@mui/icons-material/AccountCircle";
import MailIcon from "@mui/icons-material/Mail";
import NotificationsIcon from "@mui/icons-material/Notifications";

import { Breadcrumbs } from "@mui/material";
import { useRouter } from "next/router";

function handleClick(event) {
  event.preventDefault();
}

export default function Navbar({ itemCount, cart,setCart }) {

  const router = useRouter();
  const [anchorEl, setAnchorEl] = useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = useState(null);
  const [cartCount, setCartCount] = useState(1);

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const handleDeleteCart = (item) => {
    const updatedCart = cart.filter((cartItem) => cartItem.id !== item.id);
    setCart(updatedCart);
    console.log("item DELETE=>>>", item);
  };

  const calculateTotalPrice = (cart, itemCount) => {
    let totalPrice = 0;

    cart.forEach((item) => {
      const itemTotalPrice = item.price * itemCount;
      totalPrice += itemTotalPrice;
    });

    return totalPrice;
  };

  const totalCartPrice = calculateTotalPrice(cart, cartCount);

  const menuId = "primary-search-account-menu";
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      id={menuId}
      keepMounted
      transformOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem
      // onClick={handleMenuClose}
      >
        <div className="flex flex-col space-y-4">
          {cart &&
            cart.map((item, index) => (
              <div key={index}>
                <div className="flex gap-4 items-center w-full">
                  <img src={item.image} className="w-[5rem] h-[5rem]"></img>
                  <div className="flex flex-col gap-3 w-full">
                    <div className=" flex flex-wrap">{item.title}</div>
                    <div className="flex justify-between">
                      <div className="flex gap-2">
                        <div
                          className="cursor-pointer"
                          onClick={() => setCartCount(cartCount - 1)}
                        >
                          -
                        </div>
                        <div>{cartCount}</div>
                        <div
                          className="cursor-pointer"
                          onClick={() => setCartCount(cartCount + 1)}
                        >
                          +
                        </div>
                      </div>
                      <div className="font-bold">$ {item.price}</div>
                    </div>
                  </div>
                  <div
                    className="font-bold text-red-600"
                    onClick={() => {
                      handleDeleteCart(item);
                      
                    }}
                  >
                    Delete
                  </div>
                </div>
              </div>
            ))}
          <div className="flex justify-between mx-[4.1rem]">
            <div>Total:</div>
            <div>$ {totalCartPrice}</div>
          </div>
        </div>
      </MenuItem>
    </Menu>
  );

  const mobileMenuId = "primary-search-account-menu-mobile";
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{
        vertical: "top",
        horizontal: "right",
      }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem>
        <IconButton size="large" aria-label="show 4 new mails" color="inherit">
          <Badge badgeContent={4} color="error">
            <MailIcon />
          </Badge>
        </IconButton>
        <p>Messages</p>
      </MenuItem>
      <MenuItem></MenuItem>
      <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton
          size="large"
          aria-label="account of current user"
          aria-controls="primary-search-account-menu"
          aria-haspopup="true"
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
        <p>Profile</p>
      </MenuItem>
    </Menu>
  );

  return (
    <Box>
      <Box sx={{ flexGrow: 1 }}>
        <AppBar position="static">
          <Toolbar>
            <Typography
              variant="h6"
              noWrap
              component="div"
              sx={{ display: { xs: "none", sm: "block" } }}
            >
              My Shop
            </Typography>

            <Box sx={{ flexGrow: 1 }} />
            <Box sx={{ display: { xs: "none", md: "flex" } }}>
              <IconButton
                size="large"
                aria-label="show 17 new notifications"
                color="inherit"
              >
                <Badge
                  badgeContent={itemCount}
                  color="error"
                  onClick={handleProfileMenuOpen}
                  aria-controls={menuId}
                >
                  <NotificationsIcon />
                </Badge>
              </IconButton>
            </Box>
          </Toolbar>
        </AppBar>
        {renderMobileMenu}
        {renderMenu}
      </Box>

      <Box sx={{ width: "100%", typography: "body1" }}>
        <Box sx={{ mt: "10px" }}>
          <div
            role="presentation"
            onClick={handleClick}
            className="lg:px-[5rem]"
          >
            <Breadcrumbs aria-label="breadcrumb">
              <Box
                color="inherit"
                className="bg-slate-200 shadow-md p-2 cursor-pointer hover:bg-slate-300"
                onClick={() => router.push("/Mens")}
              >
                MENS' CLOTING
              </Box>
              <Box
                color="inherit"
                className="bg-slate-200 shadow-md p-2 cursor-pointer hover:bg-slate-300"
                onClick={() => router.push("/Jeweley")}
              >
                Jewelery
              </Box>
              <Box
                color="inherit"
                className="bg-slate-200 shadow-md p-2 cursor-pointer hover:bg-slate-300"
                onClick={() => router.push("/Electroics")}
              >
                Electroics
              </Box>
              <Box
                color="inherit"
                className="bg-slate-200 shadow-md p-2 cursor-pointer hover:bg-slate-300"
                onClick={() => router.push("/Womens")}
              >
                Women'sClothing
              </Box>
            </Breadcrumbs>
          </div>
        </Box>
      </Box>
    </Box>
  );
}
