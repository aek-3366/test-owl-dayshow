import Electronics from '@/components/contentdata/electronics'
import React from 'react'

export default function index() {
  return (
    <div>
      <Electronics />
    </div>
  )
}
